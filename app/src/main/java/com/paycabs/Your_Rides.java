package com.paycabs;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.paycabs.Adapter.Emergency_listviewAdapter;
import com.paycabs.Adapter.YourRides_listviewAdapter;
import com.paycabs.requestparser.Emergency_List_Request;
import com.paycabs.requestparser.YourRides_Requestparser;
import com.paycabs.responseparser.Booking;
import com.paycabs.responseparser.Emergency_List_Response;
import com.paycabs.responseparser.ShowCoontacts;
import com.paycabs.responseparser.YourRides_Responseparser;
import com.paycabs.servicecall.ServiceCall;
import com.paycabs.utils.CommonMethod;
import com.paycabs.utils.Constant;

import org.apache.http.entity.StringEntity;

import java.util.List;

public class Your_Rides extends AppCompatActivity {
    ListView simpleList;
    Toolbar toolbartop;
    String userid;
    List<Booking> list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your__rides);
        toolbartop =(Toolbar) findViewById(R.id.toolbar_yourrides);
        toolbartop.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbartop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        simpleList = (ListView) findViewById(R.id.listView);
        userid= CommonMethod.getSavedPreferencesPassengerID(Your_Rides.this);
        new YourRidesSync().execute();




    }


    private class YourRidesSync extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private YourRides_Responseparser mYourRides_Responseparser;
        private String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(Your_Rides.this, "", "Loading.....", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallSignupService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                mYourRides_Responseparser = gson.fromJson(response, YourRides_Responseparser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (mYourRides_Responseparser != null)
            {
                if (mYourRides_Responseparser.code.trim().equals("200")) {

                    YourRides_listviewAdapter mAll_Adapter = new YourRides_listviewAdapter(Your_Rides.this, mYourRides_Responseparser.booking);
                    simpleList.setAdapter(mAll_Adapter);
                    mAll_Adapter.notifyDataSetChanged();

                } else {
                    CommonMethod.showAlert(mYourRides_Responseparser.message.toString().trim(),
                            Your_Rides.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), Your_Rides.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallSignupService() {
            String url = Constant.BASE_URL + Constant.YOURRIDES;
            Log.e("url----------------", "" + url);
            try {
                YourRides_Requestparser mYourRides_Requestparser = new YourRides_Requestparser();
                mYourRides_Requestparser.passengerId=userid;
                Log.e("user id............",userid);



                Gson gson = new Gson();
                String requestInString = gson.toJson(mYourRides_Requestparser, YourRides_Requestparser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(Your_Rides.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());




            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}


package com.paycabs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.paycabs.requestparser.Availablecabs_Requestparser;
import com.paycabs.requestparser.DeleteRequestParser;
import com.paycabs.responseparser.Availablecabs_Responseparser;
import com.paycabs.responseparser.AvaliableCab;
import com.paycabs.responseparser.DeleteResponseparser;
import com.paycabs.responseparser.GetLaterBooking;
import com.paycabs.servicecall.ServiceCall;
import com.paycabs.utils.CommonMethod;
import com.paycabs.utils.Constant;

import org.apache.http.entity.StringEntity;

import java.util.List;

/**
 * Created by iSiwal on 1/19/2018.
 */

public class MyScheduleRides_Adapter extends BaseAdapter {
    Context context;
    List<GetLaterBooking> list;


    public MyScheduleRides_Adapter(Context context, List<GetLaterBooking> list)
    {
        this.context = context;
        this.list=list;
    }



    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        MyScheduleRides_Adapter.Holder holder = null;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.list_schedule, null);
            holder = new MyScheduleRides_Adapter.Holder();
            holder.time=(TextView)convertView.findViewById(R.id.later_Times);
            holder.Date=(TextView)convertView.findViewById(R.id.later_date);
            holder.source=(TextView)convertView.findViewById(R.id.later_source);
            holder.destination=(TextView)convertView.findViewById(R.id.later_destination);
            holder.images_delete=(ImageView)convertView.findViewById(R.id.img_delete);
            holder.images_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Integer index = (Integer) v.getTag();
                    //items.remove(index.intValue());
                    list.remove(position);
                    notifyDataSetChanged();
                    new deleterow().execute();
                }
            });

            convertView.setTag(holder);
        }
        else
        {
            holder = (MyScheduleRides_Adapter.Holder) convertView.getTag();
        }

        holder.time.setText(list.get(position).getRideTime());
        holder.Date.setText(list.get(position).getRideDate());
        holder.source.setText(list.get(position).getSource());
        holder.destination.setText(list.get(position).getDestination());


        return convertView;

    }

    public class Holder {
        TextView time,Date,source,destination;
        ImageView images_delete;


    }

    private class deleterow extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private DeleteResponseparser deleteResponseparser;
        private String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(context, "", "Deleting...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallLoginService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                deleteResponseparser = gson.fromJson(response, DeleteResponseparser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (deleteResponseparser != null) {
                if (deleteResponseparser.code.trim().equals("200")) {

                    Toast.makeText(context, deleteResponseparser.message, Toast.LENGTH_SHORT).show();

                } else {
                    CommonMethod.showAlert(deleteResponseparser.message.toString().trim(),
                            (Activity) context);
                }
             }
//          else {
//                CommonMethod.showAlert(getResources().getString(R.string.connection_error), context);
//            }
        }


        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallLoginService() {
            String url = Constant.BASE_URL + Constant.DeletMySchedule;
            Log.e("url----------------", "" + url);
            try {
                String booking=CommonMethod.getsaveLaterBookingid(context);
                String passenger=CommonMethod.getSavedPreferencesPassengerID(context);
                DeleteRequestParser deleteRequestParser = new DeleteRequestParser();
                String bookingid=deleteRequestParser.bookingId= booking;
                String pasen=deleteRequestParser.userId=passenger;
                Log.e("Booking id----","" + bookingid);
                Log.e("Passenger id----","" + pasen);
                Log.e("get P id----","" + passenger);
                Log.e("get B id----","" + booking);

                Gson gson = new Gson();

                String requestInString = gson.toJson(deleteRequestParser, DeleteRequestParser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(context, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}

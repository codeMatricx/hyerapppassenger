package com.paycabs.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.paycabs.R;
import com.paycabs.responseparser.Booking;
import com.paycabs.responseparser.ShowCoontacts;

import java.util.List;

/**
 * Created by iSiwal on 11/23/2017.
 */

public class YourRides_listviewAdapter extends BaseAdapter
{
    Context context;
    List<Booking> list;


    public YourRides_listviewAdapter(Context context, List<Booking> list)
    {
        this.context = context;
        this.list=list;
    }



    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        Holder holder = null;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.activity_custom, null);
            holder = new Holder();
            holder.time=(TextView)convertView.findViewById(R.id.txt_Time);
            holder.price=(TextView)convertView.findViewById(R.id.txt_price);
            holder.cartype=(TextView)convertView.findViewById(R.id.txt_cartype);
            holder.carnumber=(TextView)convertView.findViewById(R.id.txt_carnumber);
            holder.source=(TextView)convertView.findViewById(R.id.txt_source);
            holder.destination=(TextView)convertView.findViewById(R.id.txt_destination);






            convertView.setTag(holder);
        }
        else
        {
            holder = (Holder) convertView.getTag();
        }

        holder.time.setText(list.get(position).getTime());
        holder.price.setText(list.get(position).getFare());
        holder.cartype.setText(list.get(position).getVehicleType());

        holder.carnumber.setText(list.get(position).getVehicleNumber());
        holder.source.setText(list.get(position).getSource());
        holder.destination.setText(list.get(position).getDestination());


        return convertView;

    }

    public class Holder {
        TextView time,price,cartype,carnumber,source,destination;
        ImageView images_delete,img_call;

    }
}
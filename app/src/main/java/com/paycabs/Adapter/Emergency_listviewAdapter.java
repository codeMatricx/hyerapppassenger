package com.paycabs.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.paycabs.After_Splash;
import com.paycabs.MainActivity;
import com.paycabs.R;
import com.paycabs.requestparser.DeleteEmergencyContact_Request;
import com.paycabs.requestparser.DeleteRequestParser;
import com.paycabs.responseparser.DeleteEmergencycontact_Response;
import com.paycabs.responseparser.DeleteResponseparser;
import com.paycabs.responseparser.ShowCoontacts;
import com.paycabs.servicecall.ServiceCall;
import com.paycabs.utils.CommonMethod;
import com.paycabs.utils.Constant;

import org.apache.http.entity.StringEntity;

import java.util.List;

/**
 * Created by iSiwal on 11/23/2017.
 */

public class Emergency_listviewAdapter extends BaseAdapter
{
    Context context;
    List<ShowCoontacts> list;
    String str_phonenumber;

    public Emergency_listviewAdapter(Context context, List<ShowCoontacts> list)
    {
        this.context = context;
        this.list=list;
    }



    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        Holder holder = null;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.new_list_adapter, null);
            holder = new Holder();
            holder.name=(TextView)convertView.findViewById(R.id.tv_username);
            holder.phone=(TextView)convertView.findViewById(R.id.tv_phonenumber);
            holder.images_delete=(ImageView)convertView.findViewById(R.id.img_delete);
            holder.images_delete.setTag(position);
            holder.img_call=(ImageView)convertView.findViewById(R.id.img_call);
            holder.img_call.setTag(position);

            holder.images_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Integer index = (Integer) v.getTag();
                    //items.remove(index.intValue());
                    list.remove(position);
                    notifyDataSetChanged();
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    alertDialogBuilder.setMessage("Are you sure want to Delete ?");
                    alertDialogBuilder.setPositiveButton("yes",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    new Deletecontact().execute();
                                }
                            });
                    alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });


                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }

            });
            holder.img_call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + list.get(position).contactPhone));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });


            convertView.setTag(holder);
        }
        else
        {
            holder = (Holder) convertView.getTag();
        }

        holder.name.setText(list.get(position).contactName);
        Log.e("holdername -- ", "" + list.get(position).getContactName());
        holder.phone.setText(list.get(position).getContactPhone());
        Log.e("holdernumber -- ", "" + list.get(position).contactPhone);

        return convertView;

    }

    public class Holder {
        TextView name,phone;
        ImageView images_delete,img_call;

    }

    private class Deletecontact extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private DeleteEmergencycontact_Response deleteEmergencycontact_response;
        private String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(context, "", "Deleting...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallLoginService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                deleteEmergencycontact_response = gson.fromJson(response, DeleteEmergencycontact_Response.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (deleteEmergencycontact_response != null) {
                if (deleteEmergencycontact_response.code.trim().equals("200")) {

                    //Toast.makeText(context, deleteResponseparser.message, Toast.LENGTH_SHORT).show();

                } else {
                    CommonMethod.showAlert(deleteEmergencycontact_response.message.toString().trim(),
                            (Activity) context);
                }
            }

        }


        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallLoginService() {
            String url = Constant.BASE_URL + Constant.DeleteEmergencyContact;
            Log.e("url----------------", "" + url);
            try {
                String userid_E=CommonMethod.getsaveUseridemergency(context);
                String contact_id=CommonMethod.getsaveContactidemergency(context);
                DeleteEmergencyContact_Request deleteEmergencyContact_request = new DeleteEmergencyContact_Request();
                String Contact_Emer=deleteEmergencyContact_request.contactId= contact_id;
                String Userid_Emer=deleteEmergencyContact_request.userId=userid_E;
                Log.e("Booking id----","" + Contact_Emer);
                Log.e("Passenger id----","" + Userid_Emer);


                Gson gson = new Gson();
                String requestInString = gson.toJson(deleteEmergencyContact_request, DeleteEmergencyContact_Request.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(context, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}
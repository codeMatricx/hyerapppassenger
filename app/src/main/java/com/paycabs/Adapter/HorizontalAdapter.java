package com.paycabs.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.paycabs.R;
import com.paycabs.utils.CommonMethod;

import java.util.Collections;
import java.util.List;

import static android.graphics.Color.GREEN;
import static com.paycabs.R.drawable.check;
import static com.paycabs.R.drawable.imgg;
import static com.paycabs.R.drawable.mine;

/**
 * Created by Rakx on 1/4/2017.
 */
public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.MyViewHolder> {


    List<Data> horizontalList = Collections.emptyList();
    Context context;
    public static int selected_item ;
    String list;
    boolean click = true;



    public HorizontalAdapter(List<Data> horizontalList, Context context) {
        this.horizontalList = horizontalList;
        this.context = context;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView txtview,tv_itemname;
        public MyViewHolder(View view) {
            super(view);
            imageView=(ImageView) view.findViewById(R.id.img_imageview);
            txtview=(TextView) view.findViewById(R.id.tv_itemname);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view1) {

                }
            });

        }
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.vertical, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.imageView.setImageResource(horizontalList.get(position).imageId);
        holder.txtview.setText(horizontalList.get(position).txt);

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {

                  list = horizontalList.get(position).txt.toString();
                Toast.makeText(context, list, Toast.LENGTH_SHORT).show();
                CommonMethod.saveCabname(context, list);
                Log.e("cabname++++", list);
//                int id = v.getId();
//                if(position==1){
//                    holder.imageView.setBackgroundColor(Color.GREEN);
//                }
//                else{
//                    holder.imageView.setBackgroundColor(Color.WHITE);
//                }
//                if(position==2){
//                    holder.imageView.setBackgroundColor(Color.GREEN);
//                }
//                else{
//                    holder.imageView.setBackgroundColor(Color.WHITE);
//                }

            }
        });



    }


    @Override
    public int getItemCount()
    {
        return horizontalList.size();
    }
}



package com.paycabs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.identity.intents.Address;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.paycabs.requestparser.SignupRequestParser;
import com.paycabs.responseparser.SignupResponseParser;
import com.paycabs.servicecall.ServiceCall;
import com.paycabs.utils.CommonMethod;
import com.paycabs.utils.Constant;

import org.apache.http.entity.StringEntity;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static com.paycabs.R.drawable.location;


/**
 * Created by iSiwal on 9/22/2017.
 */

public class Register extends Activity implements LocationListener {
    Button btnnext, btnregister;
    Toolbar toolbartop;
    private static final int REQUEST_CODE_PERMISSION = 2;
    Marker mCurrLocationMarker;
    private GoogleMap mMap;
    // GPSTracker class
    GPSTracker gps;
    Button getloc;
    double latitude;
    double longitude;
    String address;
    private static final String TAG = "";
    Context context;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    private LocationManager locationManager;
    String mobile, pass, name, emailid;
    EditText et_namee, et_phone, et_password, et_emailid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_register);
            toolbartop = (Toolbar) findViewById(R.id.toolbar_register);
            toolbartop.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
            toolbartop.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            et_namee = (EditText) findViewById(R.id.et_name);
            et_phone = (EditText) findViewById(R.id.et_mobile);
            et_emailid = (EditText) findViewById(R.id.et_email);
            et_password = (EditText) findViewById(R.id.et_password);
            btnnext = (Button) findViewById(R.id.btn_registeraccount);
            btnnext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (et_namee.getText().toString().length() == 0) {
                        et_namee.setError("Please Enter Name");
                        et_namee.requestFocus();
                    } else if (et_emailid.getText().toString().length() == 0) {
                        et_emailid.setError("Please Enter Email Address");
                        et_emailid.requestFocus();
                    } else if (et_phone.getText().toString().length() == 0) {
                        et_phone.setError("Please Enter Phone Number");
                        et_phone.requestFocus();
                    } else if (et_password.getText().toString().length() <= 6) {
                        et_password.setError("Please Enter Password with minimum 6 digits");
                        et_password.requestFocus();
                    } else {
                        new SignupAsync().execute();

                    }

                }

//
            });


        }



    @Override
    public void onLocationChanged(Location location) {
        Log.d("onLocationChanged", "entered");
        Geocoder geocoder;
        List<android.location.Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        latitude = location.getLatitude();
        longitude = location.getLongitude();

        Log.e("Register_Lat", "" + latitude);
        Log.e("Register_Long", "" + latitude);

        try {
            Log.e("latitude", "inside latitude--" + latitude);
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null && addresses.size() > 0) {
                address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                CommonMethod.saveRide_source(getApplicationContext(), address);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title(address);


        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_location_green));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(9));
        Toast.makeText(Register.this, "Your Current Location", Toast.LENGTH_LONG).show();

        // Log.d("onLocationChanged", String.format("latitude:%.3f longitude:%.3f",latitude,longitude));

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            Log.d("onLocationChanged", "Removing Location Updates");
        }
        Log.d("onLocationChanged", "Exit");

    }

    private class SignupAsync extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private SignupResponseParser mSignupResponseParser;
        private String response = "";
        boolean valid = true;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(Register.this, "", "Creating your profile...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallSignupService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                mSignupResponseParser = gson.fromJson(response, SignupResponseParser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (mSignupResponseParser != null) {
                if (mSignupResponseParser.code.trim().equals("200")) {
                    Intent intent = new Intent(Register.this, Activity_otp.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    Toast.makeText(Register.this, mSignupResponseParser.message, Toast.LENGTH_SHORT).show();

                } else {
                    CommonMethod.showAlert(mSignupResponseParser.message.toString().trim(),
                            Register.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), Register.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallSignupService() {
            String url = Constant.BASE_URL + Constant.SIGNUP;
            Log.e("url----------------", "" + url);

            try {
                SignupRequestParser mSignupRequestParser = new SignupRequestParser();
                name=mSignupRequestParser.name = et_namee.getText().toString().trim();
                mobile=mSignupRequestParser.phone = et_phone.getText().toString().trim();
                pass=mSignupRequestParser.password = et_password.getText().toString().trim();
                emailid = mSignupRequestParser.email= et_emailid.getText().toString().trim();

                CommonMethod.savePhone(getApplicationContext(),mobile);

                Log.e("name----------------","" + name);
                Log.e("phone----------------","" + mobile);
                Log.e("passwor----------------","" + pass);
                Log.e("email----------------","" + emailid);

                Gson gson = new Gson();
                String requestInString = gson.toJson(mSignupRequestParser, SignupRequestParser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(Register.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());




            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;

        }

    }


}

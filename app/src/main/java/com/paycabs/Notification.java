package com.paycabs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;

public class Notification extends AppCompatActivity {
Toolbar toolbartop;
    Switch mySwitch,defaultSwitch;
    LinearLayout linear;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
//        getSupportActionBar().hide();
        toolbartop =(Toolbar) findViewById(R.id.toolbar_noti);
        toolbartop.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbartop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        defaultSwitch=(Switch)findViewById(R.id.default_switch);
        linear=(LinearLayout)findViewById(R.id.linear_notification);
        defaultSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){

                    linear.setVisibility(View.VISIBLE);
                }
                else {
                    linear.setVisibility(View.INVISIBLE);

                }
            }
        });
    }
}

package com.paycabs;

import android.util.Log;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.paycabs.utils.CommonMethod;

/**
 * Created by iSiwal on 1/11/2018.
 */

public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {


    //this method will be called
    //when the token is generated
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        //now we will have the token
        String token = FirebaseInstanceId.getInstance().getToken();
        CommonMethod.saveDeviceToken(getApplicationContext(),token);
        //for now we are displaying the token in the log
        //copy it as this method is called only when the new token is generated
        //and usually new token is only generated when the app is reinstalled or the data is cleared
        Log.d("MyRefreshedToken", token);
       //Toast.makeText(MyFirebaseInstanceIdService.this, ""+token, Toast.LENGTH_SHORT).show();
    }
}

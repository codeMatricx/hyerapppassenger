package com.paycabs;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.paycabs.requestparser.Changepass_Request;
import com.paycabs.responseparser.changepassword_Response;
import com.paycabs.servicecall.ServiceCall;
import com.paycabs.utils.CommonMethod;
import com.paycabs.utils.Constant;


import org.apache.http.entity.StringEntity;

public class ChangePassword extends AppCompatActivity {
    Toolbar toolbartop;
    Button btn_update;
    EditText currentpass,newpass;
    String s_uname,s_uemail,s_uphone,s_upassword,str_Driver_id,s_userid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_change_password);
        toolbartop = (Toolbar) findViewById(R.id.toolbar_updateprofile);
        toolbartop.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbartop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        str_Driver_id= CommonMethod.getSavedPreferencesPassengerID(ChangePassword.this);
        btn_update = (Button) findViewById(R.id.btn_changepass);
        currentpass = (EditText) findViewById(R.id.et_currentpass);
        newpass = (EditText) findViewById(R.id.et_newpass);
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new changepassword().execute();
            }
        });


    }

    private class changepassword extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private changepassword_Response changepassword_response;
        private String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(ChangePassword.this, "", "Changing...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallLoginService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                changepassword_response = gson.fromJson(response, changepassword_Response.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (changepassword_response != null) {
                if (changepassword_response.responseCode.trim().equals("200")) {

                    Intent intent = new Intent(ChangePassword.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();

                } else {
                    CommonMethod.showAlert(changepassword_response.responseMessage.toString().trim(),
                            ChangePassword.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), ChangePassword.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallLoginService() {
            String url = Constant.BASE_URL + Constant.ChangePassword;
            Log.e("url----------------", "" + url);
            try {
                 Changepass_Request changepass_request = new Changepass_Request();

                s_userid=changepass_request.userId=str_Driver_id;
                s_uemail=  changepass_request.currentPassword=currentpass.getText().toString();
                s_uphone= changepass_request.newPassword=newpass.getText().toString();


                Log.e("Userid----------------","" + s_userid);
                Log.e("phone----------------","" + s_uphone);
                Log.e("passwor----------------","" + s_upassword);
                Log.e("Email----------------","" + s_uemail);



                Gson gson = new Gson();
                String requestInString = gson.toJson(changepass_request, Changepass_Request.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(ChangePassword.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());




            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}
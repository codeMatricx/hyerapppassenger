package com.paycabs;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.paycabs.Extras.Constants;
import com.paycabs.utils.SessionManager;

import java.util.HashMap;

/**
 * Created by Rakx on 1/5/2017.
 */
public class SplashActivity extends AppCompatActivity {

    AlertDialogManager alert = new AlertDialogManager();

    // Session Manager Class
    SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash_activity);
        new MyFirebaseInstanceIdService();

        int DELAY = 4000;
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this,After_Splash.class);
                startActivity(intent);
                SplashActivity.this.finish();
                //overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
            }
        }, DELAY);
    }
}

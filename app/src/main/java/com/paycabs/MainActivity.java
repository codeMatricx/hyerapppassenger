package com.paycabs;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.paycabs.Adapter.Data;
import com.paycabs.Adapter.HorizontalAdapter;
import com.paycabs.Extras.SessionManager;
import com.paycabs.requestparser.Availablecabs_Requestparser;
import com.paycabs.requestparser.Driverinroute_Requestparser;
import com.paycabs.requestparser.LocateyourDriver_Requestparser;
import com.paycabs.requestparser.Ridenowrequestparser;
import com.paycabs.responseparser.Availablecabs_Responseparser;
import com.paycabs.responseparser.AvaliableCab;
import com.paycabs.responseparser.DriverinRoute_Responseparser;
import com.paycabs.responseparser.Locatedriver_Responseparser;
import com.paycabs.responseparser.Ridenowresponseparser;
import com.paycabs.servicecall.ServiceCall;
import com.paycabs.utils.CommonMethod;
import com.paycabs.utils.Constant;
import com.paycabs.utils.Session;
import org.apache.http.entity.StringEntity;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    Session s;
    Geocoder geocoder;
    String bestProvider;
    List<Address> users = null;
    double lats;
    double lngs;
    private int PROXIMITY_RADIUS = 10000;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;
    LinearLayout linearLayout_mini;
    HorizontalAdapter horizontalAdapter;
    Geocoder geoCoder;
    HashMap<Marker, AvaliableCab> hMapMarker_Model = new HashMap<Marker, AvaliableCab>();
    List<AvaliableCab> avaliableCabs;
    Context context;
    DatePicker datePicker;
    TimePicker timePicker;
    private List<Data> data;
    RecyclerView horizontal_recycler_view ;
    private static final int PLACE_PICKER_REQUEST = 1;
    private static final int DPLACE_PICKER_REQUEST = 2;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1;
    private static String TAG = "MAP LOCATION";
    Context mContext;
    TextView tv_name,tv_email,tv_phone;
    Button btn_ridenow,btn_ridelater;

    ImageView img_edit;
    AlertDialogManager alert = new AlertDialogManager();
    // Session Manager Class
    SessionManager session;
    String ride_source,ride_desti,_ride_cartype,address,address_destination,availlats,latelang;
    DrawerLayout mDrawerLayout;
    NavigationView navigationView;
    TextView tv_source,tv_desti;
    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;
    Toolbar mToolbar;
    String Common_name,Common_email,Common_phone,cab_namees,src_latlang,desti_latlang,estimatedfare,r_time,Common_P_ID,ride_PID,R_Bookingid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        s=new Session(this);
        session = new SessionManager(getApplicationContext());
        avaliableCabs = new ArrayList<>();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mContext = this;
        checkInternetConenction();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        //Check if Google Play Services Available or not
        if (!CheckGooglePlayServices()) {
            Log.d("onCreate", "Finishing test case since Google Play Services are not available");
            finish();
        }
        else {
            Log.d("onCreate","Google Play Services available.");
        }

        tv_source=(TextView)findViewById(R.id.et_source);
        btn_ridenow = (Button)findViewById(R.id.btn_ridenow);
        session.checkLogin();

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        Common_P_ID= CommonMethod.getSavedPreferencesPassengerID(MainActivity.this);
        Common_name=CommonMethod.getSavedPreferencesUserName(MainActivity.this);

        Common_phone= CommonMethod.getSavedPreferencesPhone(MainActivity.this);
        Common_email=CommonMethod.getSavedPreferencesEmailid(MainActivity.this);
        cab_namees= CommonMethod.getSavedPreferencesCABname(MainActivity.this);
        latelang=CommonMethod.getSavedSrcLatlang(MainActivity.this);

        //Find latitude and longitude
        //new availableCabs().execute();

        Log.e("Profilename++++++",""+Common_name);
        Log.e("Profilephone++++++",""+Common_phone);
        Log.e("Profileemail++++++",""+Common_email);
        Log.e("CabType++++++",""+cab_namees);
        Log.e("Abcdefghh",""+latelang);


        horizontal_recycler_view = (RecyclerView) findViewById(R.id.horizontal_recycler_view);
        data = fill_with_data();
        // get user data from session
        horizontalAdapter = new HorizontalAdapter(data, getApplication());
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false);
        horizontal_recycler_view.setLayoutManager(horizontalLayoutManager);
        horizontal_recycler_view.setAdapter(horizontalAdapter);
        horizontal_recycler_view.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View v, int position) {


                        if (position == 0) {

                        } else if (position == 1) {


                        }
                    }
                })
        );

        btn_ridelater=(Button)findViewById(R.id.btn_ridelater);
        btn_ridelater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tv_source.length() == 0) {
                    Toast.makeText(MainActivity.this, "Enter Source", Toast.LENGTH_SHORT).show();

                } else if (tv_desti.length() == 0) {
                    Toast.makeText(MainActivity.this, "Enter Destination", Toast.LENGTH_SHORT).show();
                }

                else {
                    Intent in = new Intent(MainActivity.this, Ridelater_Activity.class);
                    startActivity(in);

                }
                    }});

        btn_ridenow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new RidenowAsync().execute();

            }
        });
        tv_desti=(TextView)findViewById(R.id.et_destination);
        tv_source.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(MainActivity.this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });
        tv_desti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(MainActivity.this), DPLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }

            }
        });
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getSupportActionBar().setTitle(getResources().getString(R.string.app_name));

        mapFragment.getMapAsync(this);



        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();

         navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header=navigationView.getHeaderView(0);
        tv_name=(TextView)header.findViewById(R.id.nav_drawer_nametv);
        tv_email=(TextView)header.findViewById(R.id.nav_drawer_tvEmail);
        tv_phone=(TextView)header.findViewById(R.id.nav_drawer_tvMobile);
        img_edit=(ImageView)header.findViewById(R.id.edit_profile);
        img_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,Edit_Profile.class);
                startActivity(i);
            }
        });
        tv_name.setText(Common_name);
        tv_phone.setText(Common_phone);
        tv_email.setText(Common_email);
    }

    private boolean checkInternetConenction() {
        // get Connectivity Manager object to check connection
        ConnectivityManager connec
                =(ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        if ( connec.getNetworkInfo(0).getState() ==
                android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() ==
                        android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() ==
                        android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

            //Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
            return true;
        }else if (
                connec.getNetworkInfo(0).getState() ==
                        android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() ==
                                android.net.NetworkInfo.State.DISCONNECTED  ) {
            alert.showAlertDialog(MainActivity.this, "Internet Connection Error",
                    "Please connect to working Internet connection", false);
            // stop executing code by return

            return false;
        }
        return false;
    }

    public List<Data> fill_with_data() {

        List<Data> data = new ArrayList<>();

        data.add(new Data(R.drawable.mine, "Mini"));
        data.add(new Data(R.drawable.suv_car, "Suv"));
        data.add(new Data(R.drawable.auto_to, "Auto"));
        data.add(new Data(R.drawable.bike, "Bike"));
        data.add(new Data(R.drawable.prime, "Rental"));

        return data;

    }



    private boolean CheckGooglePlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if(result != ConnectionResult.SUCCESS) {
            if(googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result,
                        0).show();
            }
            return false;
        }
        return true;
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        }
        else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }


    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("onLocationChanged", "entered");
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        CommonMethod.saveLattitudeSource(getApplicationContext(), latitude);
        CommonMethod.saveLongitudeSource(getApplicationContext(), longitude);

        src_latlang=(latitude+","+longitude);
        Log.e("latitude", "" + latitude);
        Log.e("longitude", "" + longitude);
        Log.e("ommmm", "" +src_latlang);

        try {
            Log.e("latitude", "inside latitude--" + latitude);
            Log.e("longitude", "inside latitude--" + longitude);
            addresses = geocoder.getFromLocation(latitude, longitude, 1);

            if (addresses != null && addresses.size() > 0) {
                address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();
                tv_source.setText(address + " " + city + " " + country);
                CommonMethod.saveRide_source(getApplicationContext(),address);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //Find latitude and longitude
        new availableCabs().execute();
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        //Place current location marker
        latitude = location.getLatitude();
        Double doubleInstancelat=new Double(latitude);
        String numberlatitudes= doubleInstancelat.toString();
        Log.e("Numberlats", ""+numberlatitudes);
        longitude = location.getLongitude();
        Double doubleInstancelang=new Double(longitude);
        String numberlongetudes = doubleInstancelang.toString();
        Log.e("Numberlangs", ""+numberlongetudes);
        availlats= (numberlatitudes+","+numberlongetudes);
        Log.e("Abcede", ""+availlats);
        CommonMethod.saveSrcLatlang(getApplicationContext(), availlats);
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title(address);
        mCurrLocationMarker = mMap.addMarker(markerOptions);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(7));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 12.0f));
        Toast.makeText(MainActivity.this,"Your Current Location", Toast.LENGTH_LONG).show();

        Log.d("onLocationChanged", String.format("latitude:%.3f longitude:%.3f",latitude,longitude));

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            Log.d("onLocationChanged", "Removing Location Updates");
        }
        Log.d("onLocationChanged", "Exit");

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                 Place place = PlacePicker.getPlace(data, this);
                 StringBuilder stBuilder = new StringBuilder();
                 String placename = String.format("%s", place.getName());
                 String latitude = String.valueOf(place.getLatLng().latitude);
                 String longitude = String.valueOf(place.getLatLng().longitude);
                 address =String.format("%s", place.getAddress());
                 stBuilder.append(address);
                 tv_source.setText(stBuilder.toString());
                 CommonMethod.saveRide_source(getApplicationContext(),address);
                 src_latlang=(latitude+","+longitude);
            }
        }

         if (requestCode == DPLACE_PICKER_REQUEST) {
            Place place = PlacePicker.getPlace(data, this);
            StringBuilder stBuilder = new StringBuilder();
            String latitudes = String.valueOf(place.getLatLng().latitude);
            String longitudes = String.valueOf(place.getLatLng().longitude);
            address_destination = String.format("%s", place.getAddress());
            stBuilder.append(address_destination);
            tv_desti.setText(stBuilder.toString());
            CommonMethod.saveRide_destination(getApplicationContext(),address_destination);
            desti_latlang=(latitudes+","+longitudes);


        }
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_instant) {
            Intent k = new Intent(MainActivity.this,Instant_Booking.class);
            startActivity(k);
        } else if (id == R.id.nav_yourrides) {
            Intent k = new Intent(MainActivity.this,Your_Rides.class);
            startActivity(k);

        } else if (id == R.id.nav_Share) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent
                    .putExtra(Intent.EXTRA_TEXT,
                            "I am using "
                                    + getString(R.string.app_name)
                                    + " App ! Why don't you try it out...\nInstall "
                                    + getString(R.string.app_name)
                                    + " now !\nhttps://play.google.com/store/apps/details?id="
                                    + getPackageName());

            sendIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name) + " App !");
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, getString(R.string.Share_App)));
            return true;

        }
        else if (id == R.id.nav_payment) {
            Intent j = new Intent(MainActivity.this,Payment.class);
            startActivity(j);
        }
        else if (id == R.id.nav_changepassword) {
            Intent k = new Intent(MainActivity.this,ChangePassword.class);
            startActivity(k);
        }
        else if (id == R.id.nav_emergency) {
            Intent k = new Intent(MainActivity.this,EmergencyContactActivity.class);
            startActivity(k);
        }else if (id == R.id.nav_logout) {
            openExitDialog();
        }
        else if (id == R.id.nav_locatedriver) {
            new LocatemyDriver().execute();
        }
        else if (id == R.id.nav_schedule) {
            Intent k = new Intent(MainActivity.this,MyScheduleRides.class);
            startActivity(k);

        }

        else if (id == R.id.nav_privacy) {
            Intent n = new Intent(MainActivity.this,Privacypolicy.class);
            startActivity(n);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

        }
    }
    public void openExitDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Are you sure want to logout ?");
                alertDialogBuilder.setPositiveButton("yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                s.setLoggedin(false);
                                startActivity(new Intent(MainActivity.this,After_Splash.class));
                                finish();
                            }
                        });
alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
    @Override
    public void onClick(DialogInterface dialog, int which) {

    }
});


        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private class RidenowAsync extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private Ridenowresponseparser mRidenowresponseparser;
        private String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(MainActivity.this, "", "Loading...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallLoginService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                mRidenowresponseparser = gson.fromJson(response, Ridenowresponseparser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (mRidenowresponseparser != null) {
                if (mRidenowresponseparser.code.trim().equals("200")) {
                       String r_source= mRidenowresponseparser.home.source;
                       String r_destination = mRidenowresponseparser.home.destination;
                         estimatedfare= mRidenowresponseparser.home.fare;
                     r_time= mRidenowresponseparser.home.time;
                    R_Bookingid = mRidenowresponseparser.home.bookingId;

                    CommonMethod.saveBookingID(getApplicationContext(),R_Bookingid);
                    CommonMethod.saveRide_fare(getApplicationContext(),estimatedfare);
                    CommonMethod.saveRide_Time(getApplicationContext(),r_time);

                    Log.e("++++FARES++ -- ",""+estimatedfare);
                    Intent intent = new Intent(MainActivity.this, Confimation.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    Toast.makeText(MainActivity.this, mRidenowresponseparser.message, Toast.LENGTH_SHORT).show();

                } else {
                    CommonMethod.showAlert(mRidenowresponseparser.message.toString().trim(),
                            MainActivity.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), MainActivity.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallLoginService() {
            String url = Constant.BASE_URL + Constant.RIDENOW;
            Log.e("url----------------", "" + url);
            try {
                Ridenowrequestparser mRidenowrequestparser = new Ridenowrequestparser();

                 ride_source=mRidenowrequestparser.source = src_latlang;
                 ride_desti=mRidenowrequestparser.destination = desti_latlang;
                _ride_cartype=mRidenowrequestparser.vehicleType="Mini";
                 ride_PID= mRidenowrequestparser.userId=Common_P_ID;


                Log.e("phone-------------","" + ride_source);
                Log.e("passwor----------------","" + ride_desti);
                Log.e("Cabtype----------------","" + _ride_cartype);
                Log.e("PassengrID-------------","" + ride_PID);

                CommonMethod.saveRide_Source(getApplicationContext(),ride_source);
                CommonMethod.saveRide_Destinations(getApplicationContext(),ride_desti);
                CommonMethod.saveRidetype(getApplicationContext(),_ride_cartype);

                Gson gson = new Gson();
                String requestInString = gson.toJson(mRidenowrequestparser, Ridenowrequestparser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(MainActivity.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());




            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        s=new Session(this);
        s.cleared();
    }

    private class availableCabs extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private Availablecabs_Responseparser availablecabs_responseparser;
        private String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(MainActivity.this, "", "Loading...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallLoginService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                availablecabs_responseparser = gson.fromJson(response, Availablecabs_Responseparser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (availablecabs_responseparser != null) {
                if (availablecabs_responseparser.code.trim().equals("200")) {

                    for (int i = 0; i < availablecabs_responseparser.avaliableCabs.size(); i++) {
                        AvaliableCab model = availablecabs_responseparser.avaliableCabs.get(i);
                        if (model.getLat() > 0 && model.getLng() > 0) {
                            double lat = Double.parseDouble(String.valueOf(model.getLat()));
                            double lon = Double.parseDouble(String.valueOf(model.getLng()));

                            Marker marker = mMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(lat, lon))
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.abc)));

                            //Add Marker to Hashmap
                            hMapMarker_Model.put(marker, model);


                        } else {
                            Toast.makeText(MainActivity.this, "Arraylist error", Toast.LENGTH_SHORT).show();
                        }
                    }


                    Toast.makeText(MainActivity.this, availablecabs_responseparser.message, Toast.LENGTH_SHORT).show();

                } else {
                    CommonMethod.showAlert(availablecabs_responseparser.message.toString().trim(),
                            MainActivity.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), MainActivity.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallLoginService() {
            String url = Constant.BASE_URL + Constant.AvailableDrivers;
            Log.e("url----------------", "" + url);
            try {
                Availablecabs_Requestparser availablecabs_requestparser = new Availablecabs_Requestparser();
                String source_lat=availablecabs_requestparser.latlangavail= src_latlang;
                Log.e("Source_latitude----","" + source_lat);
                //"28.5900979,77.3333398"
                Gson gson = new Gson();
                String requestInString = gson.toJson(availablecabs_requestparser, Availablecabs_Requestparser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(MainActivity.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }

    private class LocatemyDriver extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private Locatedriver_Responseparser locatedriver_responseparser;
        private String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(MainActivity.this, "", "My Driver...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallSignupService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                locatedriver_responseparser = gson.fromJson(response, Locatedriver_Responseparser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (locatedriver_responseparser != null) {
                if (locatedriver_responseparser.code.trim().equals("200")) {
                    Intent k = new Intent(MainActivity.this,Driver_in_Route.class);
                    startActivity(k);

                } else {
                    CommonMethod.showAlert(locatedriver_responseparser.message.toString().trim(),
                            MainActivity.this);
                }
            }
//            else {
//                CommonMethod.showAlert(getResources().getString(R.string.connection_error), MainActivity.this);
//            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallSignupService() {
            String url = Constant.BASE_URL + Constant.LocateMyDrivers;
            Log.e("url----------------", "" + url);
            try {
                String bookingid =CommonMethod.getSavedPreferencesBookingID(MainActivity.this);
                String passengerid= CommonMethod.getSavedPreferencesPassengerID(MainActivity.this);
                LocateyourDriver_Requestparser locateyourDriver_requestparser = new LocateyourDriver_Requestparser();
                locateyourDriver_requestparser.bookingId=bookingid;
                locateyourDriver_requestparser.passengerId=passengerid;
                Log.e("Bookingid Locaate  -- ", "" + bookingid);
                Log.e("Passengerid Locate -- ", "" + passengerid);

                Gson gson = new Gson();
                String requestInString = gson.toJson(locateyourDriver_requestparser, LocateyourDriver_Requestparser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(MainActivity.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());




            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}

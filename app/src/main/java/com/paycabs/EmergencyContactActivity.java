package com.paycabs;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.paycabs.Adapter.Emergency_listviewAdapter;
import com.paycabs.requestparser.EmergencyContact_Request;
import com.paycabs.requestparser.Emergency_List_Request;
import com.paycabs.responseparser.AvaliableCab;
import com.paycabs.responseparser.Emergency_List_Response;
import com.paycabs.responseparser.Emergencycontact_Response;
import com.paycabs.responseparser.ShowCoontacts;
import com.paycabs.servicecall.ServiceCall;
import com.paycabs.utils.CommonMethod;
import com.paycabs.utils.Constant;

import org.apache.http.entity.StringEntity;

import java.util.List;


/**
 * Created by RAJESH on 10/5/2017.
 */

public class EmergencyContactActivity extends AppCompatActivity {

    Button btnAdd;
    EditText etUsername,etPhone;
    String name,phone,cont_id,cont_name,cont_phone,userid,m_userid;
    ListView listview;
    Toolbar toolbartop;
    TextView vendornext;
    String productname;
    List<ShowCoontacts> list;
    Emergency_listviewAdapter adapter;

    protected void onCreate(Bundle savedInseTanceState) {
        super.onCreate(savedInseTanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_emergency_contact);
        toolbartop = (Toolbar) findViewById(R.id.toolbar_emergency);
        toolbartop.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbartop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnAdd=(Button)findViewById(R.id.addnewcontact);
        listview=(ListView)findViewById(R.id.list_item_product);
        userid=CommonMethod.getSavedPreferencesPassengerID(EmergencyContactActivity.this);
        new Emergenctlist().execute();
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = getLayoutInflater();
                View alertLayout = inflater.inflate(R.layout.alert_emergency_contact, null);
                etUsername = (EditText) alertLayout.findViewById(R.id.et_username);
                etPhone = (EditText) alertLayout.findViewById(R.id.et_email);

                AlertDialog.Builder alert = new AlertDialog.Builder(EmergencyContactActivity.this);
                alert.setTitle("New Contact");
                // this is set the view from XML inside AlertDialog
                alert.setView(alertLayout);
                // disallow cancel of AlertDialog on click of back button and outside touch
                alert.setCancelable(false);
                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getBaseContext(), "Cancel clicked", Toast.LENGTH_SHORT).show();
                    }
                });

                alert.setPositiveButton("Add", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        new Emergencycontacts().execute();

                    }
                });
                AlertDialog dialog = alert.create();
                dialog.show();
            }

        });


    }




    private class Emergencycontacts extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private Emergencycontact_Response mEmergencycontact_Response;
        private String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(EmergencyContactActivity.this, "", "Saving your Number...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallSignupService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                mEmergencycontact_Response = gson.fromJson(response, Emergencycontact_Response.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (mEmergencycontact_Response != null) {
                if (mEmergencycontact_Response.code.trim().equals("200")) {
                   cont_id= mEmergencycontact_Response.emergency.contactId;
                   cont_name= mEmergencycontact_Response.emergency.contactName;
                   cont_phone= mEmergencycontact_Response.emergency.contactPhone;

                    Log.e("++ContactID","" + cont_id);
                    Log.e("++ContactName","" + cont_name);
                    Log.e("++ContactPhone","" + cont_phone);




                } else {
                    CommonMethod.showAlert(mEmergencycontact_Response.message.toString().trim(),
                            EmergencyContactActivity.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), EmergencyContactActivity.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallSignupService() {
            String url = Constant.BASE_URL + Constant.EMERGENCY_CONTACT;
            Log.e("url----------------", "" + url);
            try {
                EmergencyContact_Request mEmergencyContact_Request = new EmergencyContact_Request();

                name=mEmergencyContact_Request.contactName=etUsername.getText().toString();
                phone=mEmergencyContact_Request.contactPhone=etPhone.getText().toString();
                mEmergencyContact_Request.userId=userid;

                Log.e("Name----------------","" + name);
                Log.e("Phone----------------","" + phone);

                Gson gson = new Gson();
                String requestInString = gson.toJson(mEmergencyContact_Request, EmergencyContact_Request.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(EmergencyContactActivity.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());




            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }

    private class Emergenctlist extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private Emergency_List_Response mEmergency_List_Response;
        private String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(EmergencyContactActivity.this, "", "Loading.....", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallSignupService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                mEmergency_List_Response = gson.fromJson(response, Emergency_List_Response.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (mEmergency_List_Response != null)
            {
                if (mEmergency_List_Response.code.trim().equals("200")) {

                    Emergency_listviewAdapter mAll_Adapter = new Emergency_listviewAdapter(EmergencyContactActivity.this, mEmergency_List_Response.showCoontacts);
                    listview.setAdapter(mAll_Adapter);
                    mAll_Adapter.notifyDataSetChanged();

                    for (int i = 0; i < mEmergency_List_Response.showCoontacts.size(); i++) {
                        ShowCoontacts model = mEmergency_List_Response.showCoontacts.get(i);
                        String userid = model.getUserId();
                        String contactid = model.getContactId();

                        Log.e("Contactid", "" + contactid);
                        Log.e("Userid", "" + userid);

                        CommonMethod.saveContactidemergency(getApplicationContext(), contactid);
                        CommonMethod.saveUseridemergency(getApplicationContext(), userid);
                    }

                } else {
                    CommonMethod.showAlert(mEmergency_List_Response.message.toString().trim(),
                            EmergencyContactActivity.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), EmergencyContactActivity.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallSignupService() {
            String url ="http://staging.isiwal.com/paycabsapi/passenger/emergencycontactshow.php";
            Log.e("url----------------", "" + url);
            try {
                Emergency_List_Request mEmergency_List_Request = new Emergency_List_Request();
                m_userid=mEmergency_List_Request.userId=userid;
                Log.e("Userid","" +m_userid);


                Gson gson = new Gson();
                String requestInString = gson.toJson(mEmergency_List_Request, Emergency_List_Request.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(EmergencyContactActivity.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());




            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}

package com.paycabs;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.paycabs.Adapter.YourRides_listviewAdapter;
import com.paycabs.requestparser.MySchedule_Requestparser;
import com.paycabs.requestparser.YourRides_Requestparser;
import com.paycabs.responseparser.Booking;
import com.paycabs.responseparser.GetLaterBooking;
import com.paycabs.responseparser.MyScheduleResponse;
import com.paycabs.responseparser.YourRides_Responseparser;
import com.paycabs.servicecall.ServiceCall;
import com.paycabs.utils.CommonMethod;
import com.paycabs.utils.Constant;

import org.apache.http.entity.StringEntity;

import java.util.List;

public class MyScheduleRides extends AppCompatActivity {
Toolbar toolbartop;
    ListView simpleList;
    String userid;
    List<GetLaterBooking> list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_my_schedule_rides);
        toolbartop =(Toolbar) findViewById(R.id.toolbar_yourrides);
        toolbartop.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbartop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        simpleList = (ListView) findViewById(R.id.listView_schdule);
        userid= CommonMethod.getSavedPreferencesPassengerID(MyScheduleRides.this);
        new myschedulerides().execute();
    }

    private class myschedulerides extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private MyScheduleResponse myScheduleResponse;
        private String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(MyScheduleRides.this, "", "Loading.....", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallSignupService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                myScheduleResponse = gson.fromJson(response, MyScheduleResponse.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (myScheduleResponse != null)
            {
                if (myScheduleResponse.code.trim().equals("200")) {

                    MyScheduleRides_Adapter mAll_Adapter = new MyScheduleRides_Adapter(MyScheduleRides.this, myScheduleResponse.getLaterBooking);
                    simpleList.setAdapter(mAll_Adapter);
                    mAll_Adapter.notifyDataSetChanged();

                } else {
                    CommonMethod.showAlert(myScheduleResponse.message.toString().trim(),
                            MyScheduleRides.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), MyScheduleRides.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallSignupService() {
            String url = Constant.BASE_URL + Constant.MyScheduleRides;
            Log.e("url----------------", "" + url);
            try {
                 MySchedule_Requestparser mySchedule_requestparser = new MySchedule_Requestparser();
                mySchedule_requestparser.userId=userid;
                Log.e("user id............",userid);



                Gson gson = new Gson();
                String requestInString = gson.toJson(mySchedule_requestparser, MySchedule_Requestparser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(MyScheduleRides.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());




            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}

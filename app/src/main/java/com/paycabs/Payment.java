package com.paycabs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class Payment extends AppCompatActivity {
Toolbar toolbartop;
    RadioGroup radioGroup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_payment);
        toolbartop = (Toolbar) findViewById(R.id.toolbar_payment);
        toolbartop.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbartop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


//        radioGroup = (RadioGroup) findViewById(R.id.radios);
//        radioGroup.clearCheck();
//        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                RadioButton rb = (RadioButton) group.findViewById(checkedId);
//                if (null != rb && checkedId > -1) {
//                    if(checkedId==1) {
//                        Toast.makeText(Payment.this, rb.getText(), Toast.LENGTH_SHORT).show();
//                    }
//                }
//
//            }
//        });
//
//    }

        final RadioGroup radio = (RadioGroup) findViewById(R.id.radios);
        radio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                View radioButton = radio.findViewById(checkedId);
                int index = radio.indexOfChild(radioButton);

                // Add logic here

                switch (index) {
                    case 0: // first button

                        Toast.makeText(getApplicationContext(), "      Online      ", 500).show();
                        startActivity(new Intent(Payment.this,PaymentGateway.class));
                        break;
                    case 1: // secondbutton

                        Toast.makeText(getApplicationContext(), "       Cash        ", 500).show();
                        onBackPressed();
                        break;
                }
            }
        });
    }

    public void onClear(View v) {
        /* Clears all selected radio buttons to default */
        radioGroup.clearCheck();
    }

    public void onSubmit(View v) {
        RadioButton rb = (RadioButton) radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());
        Toast.makeText(Payment.this, rb.getText(), Toast.LENGTH_SHORT).show();
    }


}

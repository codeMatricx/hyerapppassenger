package com.paycabs.responseparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 11/14/2017.
 */

public class PassengerResponseParser {
    @SerializedName("code")
    @Expose
    public String code;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("Update_Confirmation")
    @Expose
    public UpdateConfirmation updateConfirmation;

}

package com.paycabs.responseparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by iSiwal on 1/17/2018.
 */

public class Availablecabs_Responseparser {
    @SerializedName("code")
    @Expose
    public String code;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("avaliableCabs")
    @Expose
    public List<AvaliableCab> avaliableCabs;
}

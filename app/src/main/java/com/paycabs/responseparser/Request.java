package com.paycabs.responseparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 11/21/2017.
 */

public class Request {

    @SerializedName("passenger_phone")
    @Expose
    public String passengerPhone;
    @SerializedName("passenger_name")
    @Expose
    public String passengerName;
    @SerializedName("passenger_id")
    @Expose
    public String passengerId;
    @SerializedName("driver_id")
    @Expose
    public String driverId;
    @SerializedName("driver_name")
    @Expose
    public String driverName;
    @SerializedName("driver_phone")
    @Expose
    public String driverPhone;
    @SerializedName("vehicle_name")
    @Expose
    public String vehicleName;
    @SerializedName("vehicle_number")
    @Expose
    public String vehicleNumber;
    @SerializedName("is_driver_avaliable")
    @Expose
    public String isDriverAvaliable;
}

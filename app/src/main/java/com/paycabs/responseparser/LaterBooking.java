package com.paycabs.responseparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 1/19/2018.
 */

public class LaterBooking {
    @SerializedName("later_booking_id")
    @Expose
    public String laterBookingId;
    @SerializedName("trip_id")
    @Expose
    public String tripId;
    @SerializedName("source")
    @Expose
    public String source;
    @SerializedName("destination")
    @Expose
    public String destination;
    @SerializedName("rideDate")
    @Expose
    public String rideDate;
    @SerializedName("rideTime")
    @Expose
    public String rideTime;
    @SerializedName("fare")
    @Expose
    public String fare;
}

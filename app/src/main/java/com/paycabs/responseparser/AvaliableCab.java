package com.paycabs.responseparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 1/17/2018.
 */

public class AvaliableCab {
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("driving_license")
    @Expose
    public String drivingLicense;
    @SerializedName("password")
    @Expose
    public String password;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("aadharcard_number")
    @Expose
    public String aadharcardNumber;
    @SerializedName("pan_number")
    @Expose
    public String panNumber;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("aadhar_image")
    @Expose
    public String aadharImage;
    @SerializedName("dl_image")
    @Expose
    public String dlImage;
    @SerializedName("vehicle_name")
    @Expose
    public String vehicleName;
    @SerializedName("vehicle_number")
    @Expose
    public String vehicleNumber;
    @SerializedName("vehicle_doc")
    @Expose
    public String vehicleDoc;
    @SerializedName("lat")
    @Expose
    public Double lat;
    @SerializedName("lng")
    @Expose
    public Double lng;
    @SerializedName("date")
    @Expose
    public String date;
    @SerializedName("time")
    @Expose
    public String time;
    @SerializedName("registration_date")
    @Expose
    public String registrationDate;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("created_date_time")
    @Expose
    public String createdDateTime;
    @SerializedName("isapproved")
    @Expose
    public String isapproved;
    @SerializedName("rcNumber")
    @Expose
    public Object rcNumber;
    @SerializedName("drivingLicence")
    @Expose
    public Object drivingLicence;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDrivingLicense() {
        return drivingLicense;
    }

    public void setDrivingLicense(String drivingLicense) {
        this.drivingLicense = drivingLicense;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAadharcardNumber() {
        return aadharcardNumber;
    }

    public void setAadharcardNumber(String aadharcardNumber) {
        this.aadharcardNumber = aadharcardNumber;
    }

    public String getPanNumber() {
        return panNumber;
    }

    public void setPanNumber(String panNumber) {
        this.panNumber = panNumber;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAadharImage() {
        return aadharImage;
    }

    public void setAadharImage(String aadharImage) {
        this.aadharImage = aadharImage;
    }

    public String getDlImage() {
        return dlImage;
    }

    public void setDlImage(String dlImage) {
        this.dlImage = dlImage;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getVehicleDoc() {
        return vehicleDoc;
    }

    public void setVehicleDoc(String vehicleDoc) {
        this.vehicleDoc = vehicleDoc;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getIsapproved() {
        return isapproved;
    }

    public void setIsapproved(String isapproved) {
        this.isapproved = isapproved;
    }

    public Object getRcNumber() {
        return rcNumber;
    }

    public void setRcNumber(Object rcNumber) {
        this.rcNumber = rcNumber;
    }

    public Object getDrivingLicence() {
        return drivingLicence;
    }

    public void setDrivingLicence(Object drivingLicence) {
        this.drivingLicence = drivingLicence;
    }

}

package com.paycabs.responseparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 11/24/2017.
 */

public class ShowCoontacts {

    @SerializedName("contact_id")
    @Expose
    public String contactId;
    @SerializedName("contact_name")
    @Expose
    public String contactName;
    @SerializedName("contact_phone")
    @Expose
    public String contactPhone;
    @SerializedName("user_id")
    @Expose
    public String userId;

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}

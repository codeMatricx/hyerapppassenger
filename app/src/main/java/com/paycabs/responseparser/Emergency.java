package com.paycabs.responseparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 11/23/2017.
 */


    public class Emergency {
        @SerializedName("contact_id")
        @Expose
        public String contactId;
        @SerializedName("contact_name")
        @Expose
        public String contactName;
        @SerializedName("contact_phone")
        @Expose
        public String contactPhone;
        @SerializedName("user_id")
        @Expose
        public String userId;
    }


package com.paycabs.responseparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 11/9/2017.
 */

public  class Home {
    @SerializedName("booking_id")
    @Expose
    public String bookingId;
    @SerializedName("source")
    @Expose
    public String source;
    @SerializedName("destination")
    @Expose
    public String destination;
    @SerializedName("time")
    @Expose
    public String time;
    @SerializedName("fare")
    @Expose
    public String fare;
}

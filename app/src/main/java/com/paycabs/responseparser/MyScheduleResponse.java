package com.paycabs.responseparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by iSiwal on 1/19/2018.
 */

public class MyScheduleResponse {
    @SerializedName("code")
    @Expose
    public String code;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("getLaterBooking")
    @Expose
    public List<GetLaterBooking> getLaterBooking;


}

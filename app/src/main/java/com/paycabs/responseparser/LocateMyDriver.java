package com.paycabs.responseparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 1/25/2018.
 */

public class LocateMyDriver {
    @SerializedName("driverId")
    @Expose
    public String driverId;
    @SerializedName("isTripComplete")
    @Expose
    public String isTripComplete;
    @SerializedName("lat")
    @Expose
    public String lat;
    @SerializedName("long")
    @Expose
    public String _long;
    @SerializedName("driverName")
    @Expose
    public String driverName;
    @SerializedName("driverPhone")
    @Expose
    public String driverPhone;
    @SerializedName("vehicleName")
    @Expose
    public String vehicleName;
    @SerializedName("vehicleNumber")
    @Expose
    public String vehicleNumber;
    @SerializedName("driverAddress")
    @Expose
    public String driverAddress;
}

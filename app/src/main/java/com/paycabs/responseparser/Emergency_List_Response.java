package com.paycabs.responseparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by iSiwal on 11/24/2017.
 */

public class Emergency_List_Response {
    @SerializedName("code")
    @Expose
    public String code;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("Show Coontacts")
    @Expose
    public List<ShowCoontacts> showCoontacts;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public List<ShowCoontacts> getProductByCategory() {
        return showCoontacts;
    }

    public void setShowCoontacts(List<ShowCoontacts> showCoontacts) {
        this.showCoontacts = showCoontacts;
    }
}
package com.paycabs.responseparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 11/24/2017.
 */

public class FinalPrice_ResponseParser {
    @SerializedName("code")
    @Expose
    public String  code;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("Booking")
    @Expose
    public Booking booking;
}

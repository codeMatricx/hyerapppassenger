package com.paycabs.responseparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 1/19/2018.
 */

public class GetLaterBooking {
        @SerializedName("later_booking_id")
        @Expose
        public String laterBookingId;
        @SerializedName("source")
        @Expose
        public String source;
        @SerializedName("destination")
        @Expose
        public String destination;
        @SerializedName("rideDate")
        @Expose
        public String rideDate;
        @SerializedName("rideTime")
        @Expose
        public String rideTime;
        @SerializedName("fare")
        @Expose
        public String fare;
        public String getLaterBookingId() {
            return laterBookingId;
        }

        public void setLaterBookingId(String laterBookingId) {
            this.laterBookingId = laterBookingId;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getDestination() {
            return destination;
        }

        public void setDestination(String destination) {
            this.destination = destination;
        }

        public String getRideDate() {
            return rideDate;
        }

        public void setRideDate(String rideDate) {
            this.rideDate = rideDate;
        }

        public String getRideTime() {
            return rideTime;
        }

        public void setRideTime(String rideTime) {
            this.rideTime = rideTime;
        }

        public String getFare() {
            return fare;
        }

        public void setFare(String fare) {
            this.fare = fare;
        }
    }


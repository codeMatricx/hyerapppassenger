package com.paycabs.responseparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 11/14/2017.
 */

public class UpdateConfirmation {

    @SerializedName("booking_id")
    @Expose
    public String bookingId;
    @SerializedName("source")
    @Expose
    public String source;
    @SerializedName("destination")
    @Expose
    public String destination;
    @SerializedName("fare")
    @Expose
    public String fare;
    @SerializedName("time")
    @Expose
    public String time;
    @SerializedName("number_passenger")
    @Expose
    public String numberPassenger;
    @SerializedName("payment_mode")
    @Expose
    public String paymentMode;
    
}
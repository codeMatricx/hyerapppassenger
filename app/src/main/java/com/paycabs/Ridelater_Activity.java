package com.paycabs;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;


import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.paycabs.requestparser.Ridelater_Request;
import com.paycabs.responseparser.RideLater_Response;
import com.paycabs.servicecall.ServiceCall;
import com.paycabs.utils.CommonMethod;
import com.paycabs.utils.Constant;

import org.apache.http.entity.StringEntity;

import java.util.Calendar;

public class Ridelater_Activity extends AppCompatActivity implements View.OnClickListener {
    Button btnDatePicker, btnTimePicker,submit;
    EditText txtDate, txtTime;
    private int mYear, mMonth, mDay, mHour, mMinute;
    String later_userid,later_source,later_destination,later_bookingid,later_sources,later_destinations,later_date,later_time;
    Toolbar toolbartop;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.date_time_picker);
        toolbartop = (Toolbar) findViewById(R.id.toolbar_schedule);
        toolbartop.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbartop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        later_source=CommonMethod.getSavedPreferencesRide_source(Ridelater_Activity.this);
        later_destination=CommonMethod.getSavedPreferencesRide_destination(Ridelater_Activity.this);
        later_userid=CommonMethod.getSavedPreferencesPassengerID(Ridelater_Activity.this);
        Log.e("user",""+later_userid);
        Log.e("Source",""+later_source);
        Log.e("destination",""+later_destination);

        btnDatePicker=(Button)findViewById(R.id.btn_date);
        btnTimePicker=(Button)findViewById(R.id.btn_time);
        txtDate=(EditText)findViewById(R.id.in_date);
        txtTime=(EditText)findViewById(R.id.in_time);
        submit=(Button)findViewById(R.id.submit_ridelater);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtDate.getText().toString().length()==0){
                    txtDate.setError("Select Date");
                    txtDate.requestFocus();
                }

                else if(txtTime.getText().toString().length()==0){
                    txtTime.setError("Select Time");
                    txtTime.requestFocus();
                }


                else{
                    new RideLater().execute();
                }

            }
        });

        btnDatePicker.setOnClickListener(this);
        btnTimePicker.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if (v == btnDatePicker) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
        if (v == btnTimePicker) {

            // Get Current Time
            Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            txtTime.setText(hourOfDay + ":" + minute);
                        }
                    }, mHour, mMinute, false);
            timePickerDialog.show();
        }
    }

    private class RideLater extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private RideLater_Response rideLater_response;
        private String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(Ridelater_Activity.this, "", "Please Wait...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallLoginService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                rideLater_response = gson.fromJson(response, RideLater_Response.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (rideLater_response != null) {
                if (rideLater_response.code.trim().equals("200")) {
                    later_bookingid=rideLater_response.laterBooking.laterBookingId;
                    later_sources=rideLater_response.laterBooking.source;
                    later_destinations=rideLater_response.laterBooking.destination;
                    later_date=rideLater_response.laterBooking.rideDate;
                    later_time=rideLater_response.laterBooking.rideTime;

                    CommonMethod.saveLaterBookingid(getApplicationContext(),later_bookingid);
                    Log.e("LaterBooking",""+later_bookingid);

                    finish();
                    Toast.makeText(Ridelater_Activity.this, "Your Ride Has Been Successfully Schedule", Toast.LENGTH_SHORT).show();

                } else {
                    CommonMethod.showAlert(rideLater_response.message.toString().trim(), Ridelater_Activity.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), Ridelater_Activity.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallLoginService() {
            String url = Constant.BASE_URL + Constant.RideLater;
            Log.e("url----------------", "" + url);
            try {
                Ridelater_Request ridelater_request = new Ridelater_Request();
                String userid=ridelater_request.userId=later_userid;
                String source =ridelater_request.source=later_source;
                String destination= ridelater_request.destination=later_destination;
                String date=ridelater_request.rideDate=txtDate.getText().toString();
                String time=ridelater_request.rideTime=txtTime.getText().toString();
                String vehicletype=ridelater_request.vehicleType="Basic";

                Log.e("Later_Userid----","" + userid);
                Log.e("Later_source---","" + source);
                Log.e("Later_destination---","" + destination);
                Log.e("Later_date---","" + date);
                Log.e("Later_time---","" + time);
                Log.e("Later_vehivletype---","" + vehicletype);


                Gson gson = new Gson();
                String requestInString = gson.toJson(ridelater_request, Ridelater_Request.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(Ridelater_Activity.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}

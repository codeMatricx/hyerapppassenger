    package com.paycabs;

    import android.annotation.SuppressLint;
    import android.app.ProgressDialog;
    import android.content.Intent;
    import android.os.AsyncTask;
    import android.os.Bundle;
    import android.support.v7.app.AppCompatActivity;
    import android.support.v7.widget.Toolbar;
    import android.util.Log;
    import android.view.View;
    import android.view.WindowManager;
    import android.widget.Button;
    import android.widget.EditText;
    import android.widget.Toast;

    import com.google.gson.Gson;
    import com.google.gson.JsonIOException;
    import com.google.gson.JsonSyntaxException;
    import com.paycabs.requestparser.UpdateRequestparser;
    import com.paycabs.responseparser.UpdateResponseparser;
    import com.paycabs.servicecall.ServiceCall;
    import com.paycabs.utils.CommonMethod;
    import com.paycabs.utils.Constant;


    import org.apache.http.entity.StringEntity;

    public class Edit_Profile extends AppCompatActivity {
        Toolbar toolbartop;
        Button btn_update;
        EditText et_uname,et_uemial,et_upassword,et_uphone;
        String s_uname,s_uemail,s_uphone,s_upassword,st_Passenger_id,s_userid;
        String Common_name,Common_phone,Common_email,Common_password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit__profile);
        toolbartop = (Toolbar) findViewById(R.id.toolbar_updateprofile);
        toolbartop.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbartop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btn_update=(Button)findViewById(R.id.btn_update_profile);
        et_uname=(EditText)findViewById(R.id.et_update_name);
        et_uemial=(EditText)findViewById(R.id.et_update_email);
        et_uphone=(EditText)findViewById(R.id.et_update_mobile);

        Common_name=CommonMethod.getSavedPreferencesUserName(Edit_Profile.this);
        Common_phone= CommonMethod.getSavedPreferencesPhone(Edit_Profile.this);
        Common_email=CommonMethod.getSavedPreferencesEmailid(Edit_Profile.this);
        st_Passenger_id= CommonMethod.getSavedPreferencesPassengerID(Edit_Profile.this);

        et_uname.setText(Common_name);
        et_uemial.setText(Common_email);
        et_uphone.setText(Common_phone);


        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new updateprofile().execute();
            }
        });


    }

        private class updateprofile extends AsyncTask<String, Void, String> {
            private ProgressDialog progress;
            private UpdateResponseparser mUpdateResponseparser;
            private String response = "";

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progress = ProgressDialog.show(Edit_Profile.this, "", "Updating...", true);
                progress.setCancelable(true);
            }

            @Override
            protected String doInBackground(String... params) {
                response = CallLoginService();
                return response;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                progress.dismiss();

                try {
                    // get response from server side and store in SignupResponse Parser///////
                    Gson gson = new Gson();
                    mUpdateResponseparser = gson.fromJson(response, UpdateResponseparser.class);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                } catch (JsonIOException e) {
                    e.printStackTrace();
                }
                if (mUpdateResponseparser != null) {
                    if (mUpdateResponseparser.code.trim().equals("200")) {

                        String update_name = mUpdateResponseparser.data.name;
                        String update_email = mUpdateResponseparser.data.email;
                        String update_phone = mUpdateResponseparser.data.phone;

                        CommonMethod.saveUserName(getApplicationContext(),update_name);
                        CommonMethod.saveEmailid(getApplicationContext(),update_email);
                        CommonMethod.savePhone(getApplicationContext(),update_phone);
                        Intent intent = new Intent(Edit_Profile.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        Toast.makeText(Edit_Profile.this, mUpdateResponseparser.message, Toast.LENGTH_SHORT).show();

                    } else {
                        CommonMethod.showAlert(mUpdateResponseparser.message.toString().trim(),
                                Edit_Profile.this);
                    }
                } else {
                    CommonMethod.showAlert(getResources().getString(R.string.connection_error), Edit_Profile.this);
                }
            }

            @SuppressLint("DefaultLocale")

            //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

            private String CallLoginService() {
                String url = Constant.BASE_URL + Constant.UPDATEPROFILE;
                Log.e("url----------------", "" + url);
                try {
                    UpdateRequestparser mUpdateRequestparser = new UpdateRequestparser();
                    s_userid=mUpdateRequestparser.userId=st_Passenger_id;
                    s_uemail=  mUpdateRequestparser.email=et_uemial.getText().toString();
                    s_uphone= mUpdateRequestparser.phone=et_uphone.getText().toString();
                    s_uname=mUpdateRequestparser.name=et_uname.getText().toString();

                    Log.e("PassengerID-----------","" + s_userid);
                    Log.e("phone----------------","" + s_uphone);
                    Log.e("password---------------","" + s_upassword);
                    Log.e("Email----------------","" + s_uemail);
                    Log.e("Name----------------","" + s_uname);



                    Gson gson = new Gson();
                    String requestInString = gson.toJson(mUpdateRequestparser, UpdateRequestparser.class);
                    System.out.println(requestInString);
                    StringEntity stringEntity = new StringEntity(requestInString);
                    ServiceCall serviceCall = new ServiceCall(Edit_Profile.this, url, stringEntity);
                    response = serviceCall.getServiceResponse();
                    Log.e("Response -- ", "" + response);
                    System.out.println(serviceCall.getServiceResponse());




                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }
        }
    }

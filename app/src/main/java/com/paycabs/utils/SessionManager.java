package com.paycabs.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.paycabs.Login;
import com.paycabs.MainActivity;
import com.paycabs.SplashActivity;

import java.util.HashMap;



/**
 * Created by HeadStrong on 2/2/2017.
 */
public class SessionManager
{
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "PayCabs";
    private static final String IS_LOGIN = "IsLoggedIn";
    public static final String USERID = "userid";
    public static final String USERPHONE = "userphone";
    public static final String Password = "password";
    public static final String Restaurant = "restaurant";
    public static final String Hospital = "hospital";
    public static final String Extension_number = "extension_number";
    public static final String Status = "status";
    public SessionManager(Context context)
    {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
    public void createLoginSession(String userphone, String password)
    {
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(USERPHONE, userphone);
        editor.putString(Password, password);
        editor.commit();
    }
    public void checkLogin()
    {
        if(!this.isLoggedIn())
        {
            Intent i = new Intent(_context, Login.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);
        }
    }
    public HashMap<String, String> getUserDetails()
    {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(USERID, pref.getString(USERID, null));
        user.put(USERPHONE, pref.getString(USERPHONE, null));
        user.put(Password, pref.getString(Password, null));
        return user;
    }

    public void logoutUser()
    {
        editor.clear();
        editor.commit();
        Intent i = new Intent(_context, SplashActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i);
    }

    public boolean isLoggedIn()
    {
        return pref.getBoolean(IS_LOGIN, false);
    }
}


package com.paycabs.utils;

public class Constant {

	public static final String FALSE = "false";
	public static final String TXT_BLANK = "";

	// cama
	public static String UTF_8 = "UTF-8";
	public static String SERVER_RESPONSE_SUCCESS_CODE = "200";
	public static String CONTENT_TYPE = "Content-type";
	public static String APPLICATION_JSON = "application/json";
	public static String ACCESS = "Access";
	public static final String RESPONCE_CODE = "responseCode";
	public static final String RESPONCE_MESSAGE = "responseMessage";

	public static final String RESPONCECODE = "responsecode";
	public static final String RESPONCEMESSAGE = "responsemessage";

	/***  App Preference Keys */
	public static final String PREF_USERNAME 			= "fullname";
	public static final String PREF_USERID 				= "USER_ID";
	public static final String PREF_STSFF_USERID 		= "USER_ID";
	public static final String PREF_USER_IMAGE 			= "image";

	public static final String BASE_URL = "http://staging.isiwal.com/paycabsapi/passenger/";

	public static final String LOGIN = "login.php";
	public static final String SIGNUP = "register.php";
	public static final String RIDENOW = "home.php";
	public static final String NumberofPassengers = "confirmation.php";
	public static final String UPDATEPROFILE = "update.php";
	public static final String After_Confirmation = "send_request.php";
	public static final String CancelBooking = "canclebooking.php";
	public static final String FORGETPASS = "forgotpassword.php";
	public static final String EMERGENCY_CONTACT = "emergencycontact.php";
	public static final String YOURRIDES = "passenger_ride.php";
	public static final String FINALBILL ="passenger_bill.php";
	public static final String OTP ="verify_passenger.php";
	public static final String AvailableDrivers = "carMoving.php";
	public static final String RideLater = "rideLaterBooking.php";
	public static final String MyScheduleRides = "myScheduledRide.php";
	public static final String DeletMySchedule = "deleteLaterRide.php";
	public static final String DriverInRoute = "driverInRoute.php";
	public static final String LocateMyDrivers = "locateMyDriver.php";
	public static final String DeleteEmergencyContact = "delete_emergency_contact.php";
	public static final String ChangePassword = "changePassword.php";


}

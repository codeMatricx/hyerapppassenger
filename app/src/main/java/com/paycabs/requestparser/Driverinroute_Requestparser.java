package com.paycabs.requestparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 1/24/2018.
 */

public class Driverinroute_Requestparser {
    @SerializedName("booking_id")
    @Expose
    public String bookingId;
    @SerializedName("passenger_id")
    @Expose
    public String passengerId;
}

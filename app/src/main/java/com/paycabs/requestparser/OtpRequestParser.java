package com.paycabs.requestparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 1/12/2018.
 */

public class OtpRequestParser  {

    @SerializedName("otp")
    @Expose
    public String otp;
    @SerializedName("phone")
    @Expose
    public String phone;
}

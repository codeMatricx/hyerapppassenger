package com.paycabs.requestparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 11/24/2017.
 */

public class YourRides_Requestparser {

    @SerializedName("passenger_id")
    @Expose
    public String passengerId;
}

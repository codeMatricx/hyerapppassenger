package com.paycabs.requestparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by alobha on 2/9/16.
 */
public class LoginRequestParser {

    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("password")
    @Expose
    public String password;
    @SerializedName("device_type")
    @Expose
    public String deviceType;
    @SerializedName("device_token")
    @Expose
    public String deviceToken;


}

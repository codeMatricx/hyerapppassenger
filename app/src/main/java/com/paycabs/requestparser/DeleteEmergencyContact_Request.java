package com.paycabs.requestparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 1/25/2018.
 */

public class DeleteEmergencyContact_Request {
    @SerializedName("contact_id")
    @Expose
    public String contactId;
    @SerializedName("user_id")
    @Expose
    public String userId;
}

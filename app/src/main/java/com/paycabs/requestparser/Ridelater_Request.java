package com.paycabs.requestparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 1/19/2018.
 */

public class Ridelater_Request {
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("source")
    @Expose
    public String source;
    @SerializedName("destination")
    @Expose
    public String destination;
    @SerializedName("vehicle_type")
    @Expose
    public String vehicleType;
    @SerializedName("rideDate")
    @Expose
    public String rideDate;
    @SerializedName("rideTime")
    @Expose
    public String rideTime;

}

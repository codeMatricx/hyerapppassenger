package com.paycabs.requestparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 1/25/2018.
 */

public class Changepass_Request {
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("currentPassword")
    @Expose
    public String currentPassword;
    @SerializedName("newPassword")
    @Expose
    public String newPassword;
}

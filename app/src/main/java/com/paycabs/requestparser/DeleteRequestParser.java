package com.paycabs.requestparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 1/22/2018.
 */

public class DeleteRequestParser {
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("booking_id")
    @Expose
    public String bookingId;
}

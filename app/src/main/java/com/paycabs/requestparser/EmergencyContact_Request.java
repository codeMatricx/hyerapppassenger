package com.paycabs.requestparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 11/23/2017.
 */

public class EmergencyContact_Request {


    @SerializedName("contact_name")
    @Expose
    public String contactName;
    @SerializedName("contact_phone")
    @Expose
    public String contactPhone;
    @SerializedName("user_id")
    @Expose
    public String userId;
}

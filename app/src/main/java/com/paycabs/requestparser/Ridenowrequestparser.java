package com.paycabs.requestparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 11/7/2017.
 */

public class Ridenowrequestparser {
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("source")
    @Expose
    public String source;
    @SerializedName("destination")
    @Expose
    public String destination;
    @SerializedName("vehicle_type")
    @Expose
    public String vehicleType;




}

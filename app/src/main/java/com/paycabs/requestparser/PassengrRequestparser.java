package com.paycabs.requestparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 11/14/2017.
 */

public class PassengrRequestparser {
    @SerializedName("booking_id")
    @Expose
    public String bookingId;
    @SerializedName("number_passenger")
    @Expose
    public String numberPassenger;
    @SerializedName("ride_type")
    @Expose
    public String rideType;
    @SerializedName("payment_mode")
    @Expose
    public String paymentMode;


}

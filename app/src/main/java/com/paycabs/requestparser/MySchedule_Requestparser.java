package com.paycabs.requestparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 1/19/2018.
 */

public class MySchedule_Requestparser {
    @SerializedName("userId")
    @Expose
    public String userId;
}

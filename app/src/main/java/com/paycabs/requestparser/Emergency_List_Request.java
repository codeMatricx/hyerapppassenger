package com.paycabs.requestparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 11/23/2017.
 */

public class Emergency_List_Request
{
        @SerializedName("user_id")
        @Expose
        public String userId;

}

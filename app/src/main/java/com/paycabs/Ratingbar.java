package com.paycabs;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.paycabs.utils.CommonMethod;

public class Ratingbar extends AppCompatActivity {
    Toolbar toolbartop;
    Button btn_rating;
    RatingBar rating;
    String finalbill;
    TextView tv_fare,online,cash;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_ratingbar);
        toolbartop = (Toolbar) findViewById(R.id.toolbar_ratingbars);
        toolbartop.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbartop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        addListenerOnButtonClick();
        online=(TextView)findViewById(R.id.tv_online);
        cash=(TextView)findViewById(R.id.tv_cash);
        finalbill=CommonMethod.getSavedPreferencesFinalBill(Ratingbar.this);
        tv_fare=(TextView)findViewById(R.id.final_prize);
        tv_fare.setText(finalbill);
        online.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Ratingbar.this,PaymentGateway.class));
            }
        });
        cash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder=new AlertDialog.Builder(Ratingbar.this);
                builder.setCancelable(true).setMessage("Pay cash to Driver").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                //Setting the title manually
                alert.setTitle("Cash Payment");
                alert.show();
            }
        });
    }

    private void addListenerOnButtonClick() {
        rating=(RatingBar)findViewById(R.id.ratingBar1);
        btn_rating=(Button)findViewById(R.id.rating_submit_btn);
        //Performing action on Button Click
        btn_rating.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View arg0) {
                //Getting the rating and displaying it on the toast
                String ratings=String.valueOf(rating.getRating());
                Toast.makeText(getApplicationContext(), ratings, Toast.LENGTH_LONG).show();
                Intent k = new Intent(Ratingbar.this,MainActivity.class);
                startActivity(k);
                finish();
            }

        });
    }
}

package com.paycabs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.paycabs.Extras.SessionManager;
import com.paycabs.requestparser.LoginRequestParser;
import com.paycabs.requestparser.SignupRequestParser;
import com.paycabs.responseparser.LoginResponseParser;

import com.paycabs.servicecall.ServiceCall;
import com.paycabs.utils.CommonMethod;
import com.paycabs.utils.Constant;
import com.paycabs.utils.Session;


import org.apache.http.entity.StringEntity;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


/**
 * Created by iSiwal on 9/22/2017.
 */

public class Login extends Activity implements LocationListener {
    Button btnsign;
    Toolbar toolbartop;
    EditText phone, pass;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private Session set_s;
    String l_phone,l_password,l_name,l_emial,l_phonee,st_passengr_id,st_password,devicetoken,l_devicetoken,src_latlang,address,availlats;
    private static final String TAG = "MainActivity";

    AlertDialogManager alert = new AlertDialogManager();
    SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        set_s=new Session(this);
       session = new SessionManager(getApplicationContext());
        session = new SessionManager(Login.this);
        toolbartop = (Toolbar) findViewById(R.id.toolbar_login);
        toolbartop.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbartop.setTitle("Login PayCabs");
        toolbartop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        devicetoken=CommonMethod.getSavedDeviceToken(Login.this);


        phone = (EditText) findViewById(R.id.et_phone);
        pass = (EditText) findViewById(R.id.et_pass);
        btnsign = (Button) findViewById(R.id.btnLogin);


        btnsign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //login();
                if(phone.getText().toString().length()==0){
                    phone.setError("Please Enter Registered Phone number");
                    phone.requestFocus();
                }
                else if(pass.getText().toString().length()<=5){
                    pass.setError("Please Enter Valid Password");
                    pass.requestFocus();
                }
                else {
                    new SigninAsync().execute();
                }


            }
        });
        if(set_s.loggedin()){
            startActivity(new Intent(Login.this,MainActivity.class));
            finish();
        }


    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("onLocationChanged", "entered");
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        CommonMethod.saveLattitudeSource(getApplicationContext(), latitude);
        CommonMethod.saveLongitudeSource(getApplicationContext(), longitude);

        src_latlang=(latitude+","+longitude);
        Log.e("latitude", "" + latitude);
        Log.e("longitude", "" + longitude);
        Log.e("ommmm", "" +src_latlang);

        try {
            Log.e("latitude", "inside latitude--" + latitude);
            Log.e("longitude", "inside latitude--" + longitude);
            addresses = geocoder.getFromLocation(latitude, longitude, 1);

            if (addresses != null && addresses.size() > 0) {
                address = addresses.get(0).getAddressLine(0);
                CommonMethod.saveRide_source(getApplicationContext(),address);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        //Place current location marker
        latitude = location.getLatitude();
        Double doubleInstancelat=new Double(latitude);
        String numberlatitudes= doubleInstancelat.toString();
        Log.e("Numberlats", ""+numberlatitudes);
        longitude = location.getLongitude();
        Double doubleInstancelang=new Double(longitude);
        String numberlongetudes = doubleInstancelang.toString();
        Log.e("Numberlangs", ""+numberlongetudes);
        availlats= (numberlatitudes+","+numberlongetudes);
        Log.e("Abcede", ""+availlats);
        CommonMethod.saveSrcLatlang(getApplicationContext(), availlats);
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title(address);
        mCurrLocationMarker = mMap.addMarker(markerOptions);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//      mMap.animateCamera(CameraUpdateFactory.zoomTo(7));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 12.0f));
        Toast.makeText(Login.this,"Your Current Location", Toast.LENGTH_LONG).show();

        Log.d("onLocationChanged", String.format("latitude:%.3f longitude:%.3f",latitude,longitude));

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            Log.d("onLocationChanged", "Removing Location Updates");
        }
        Log.d("onLocationChanged", "Exit");

    }


    private class SigninAsync extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private LoginResponseParser mLoginResponseParser;
        private String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(Login.this, "", "Login...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallLoginService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                mLoginResponseParser = gson.fromJson(response, LoginResponseParser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (mLoginResponseParser != null) {
                if (mLoginResponseParser.code.trim().equals("200")) {
                    l_name=mLoginResponseParser.profile.name;
                    l_emial=mLoginResponseParser.profile.email;
                    l_phone=mLoginResponseParser.profile.phone;
                    st_passengr_id=mLoginResponseParser.profile.userId;
                    st_password=mLoginResponseParser.profile.password;

                    session.createLoginSession(l_phone, st_password);


                    CommonMethod.saveEmailid(getApplicationContext(),l_emial);
                    CommonMethod.savePhone(getApplicationContext(),l_phone);
                    CommonMethod.saveUserName(getApplicationContext(),l_name);
                    CommonMethod.savePassengerID(getApplicationContext(),st_passengr_id);
                    //CommonMethod.savePassword(getApplicationContext(),st_password);


                    set_s.setLoggedin(true);

                    Log.e("++++USERNAME++ -- ",""+l_name);
                    Log.e("+++USEREMAIL+ -- ",""+l_emial);
                    Log.e("++++USERPHONE++ -- ",""+l_phone);
                    Log.e("++++PASSENGERID++ -- ",""+st_passengr_id);

                    Intent intent = new Intent(Login.this, MainActivity.class);
                    intent.putExtra("Name",l_name);
                    intent.putExtra("Phone",l_phone);
                    intent.putExtra("Email",l_emial);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    Toast.makeText(Login.this, mLoginResponseParser.message, Toast.LENGTH_SHORT).show();

                } else {
                    CommonMethod.showAlert(mLoginResponseParser.message.toString().trim(),
                            Login.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), Login.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallLoginService() {
            String url = Constant.BASE_URL + Constant.LOGIN;
            Log.e("url----------------", "" + url);
            try {
                LoginRequestParser mLoginRequestParser = new LoginRequestParser();

                l_phone=mLoginRequestParser.phone = phone.getText().toString().trim();
                l_password=mLoginRequestParser.password = pass.getText().toString().trim();
                mLoginRequestParser.deviceType="android";
                l_devicetoken=mLoginRequestParser.deviceToken=devicetoken;


                Log.e("phone----------------","" + l_phone);
                Log.e("passwor----------------","" + l_password);
                Log.e("Device_token----","" + l_devicetoken);


                Gson gson = new Gson();
                String requestInString = gson.toJson(mLoginRequestParser, LoginRequestParser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(Login.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());




            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}
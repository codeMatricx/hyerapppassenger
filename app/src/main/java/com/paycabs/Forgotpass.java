package com.paycabs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.paycabs.requestparser.SignupRequestParser;
import com.paycabs.responseparser.ForgetPassResponseParser;
import com.paycabs.responseparser.SignupResponseParser;
import com.paycabs.servicecall.ServiceCall;
import com.paycabs.utils.CommonMethod;
import com.paycabs.utils.Constant;

import org.apache.http.entity.StringEntity;

public class Forgotpass extends Activity {
Toolbar toolbartop;
    Button btn_send;
    EditText et_email;
    String emailid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpass);
       // getSupportActionBar().hide();
        toolbartop =(Toolbar) findViewById(R.id.toolbar_fpass);
        toolbartop.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbartop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        et_email=(EditText)findViewById(R.id.forgetpassword);
        btn_send=(Button)findViewById(R.id.register_btn);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
                public void onClick(View v) {
                new forgetAsync().execute();
//
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class forgetAsync extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private ForgetPassResponseParser mforgetresponseparser;
        private String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(Forgotpass.this, "", "Please wait...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallSignupService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                mforgetresponseparser = gson.fromJson(response, ForgetPassResponseParser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (mforgetresponseparser != null) {
                if (mforgetresponseparser.code.trim().equals("200")) {

                    AlertDialog.Builder dialog = new AlertDialog.Builder(Forgotpass.this);
                    dialog.setCancelable(false);
                    dialog.setTitle("Thank You !!!");
                    dialog.setMessage("Your password has been changed Successfully, Please check your Registered Email Id");
                    dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Forgotpass.this, Login.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        Toast.makeText(Forgotpass.this, mforgetresponseparser.message, Toast.LENGTH_SHORT).show();
                    }
                });


                AlertDialog alertDialog = dialog.create();
                alertDialog.show();




                } else {
                    CommonMethod.showAlert(mforgetresponseparser.message.toString().trim(),
                            Forgotpass.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), Forgotpass.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallSignupService() {
            String url = Constant.BASE_URL + Constant.FORGETPASS;
            Log.e("url----------------", "" + url);
            try {
                SignupRequestParser mSignupRequestParser = new SignupRequestParser();

                emailid = mSignupRequestParser.email= et_email.getText().toString().trim();

                Log.e("email----------------","" + emailid);

                Gson gson = new Gson();
                String requestInString = gson.toJson(mSignupRequestParser, SignupRequestParser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(Forgotpass.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());




            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}
